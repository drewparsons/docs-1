.. title:: FEniCS Project

==============
FEniCS Project
==============

This is experimental documentation for the `FEniCS Project
<https://fenicsproject.org>`_.  This version of the documentation on
Read the Docs is under development.

.. toctree::
   :maxdepth: 2

   installation
   getting_started
   developer


Documentation for components
============================

FEniCS is a collection of inter-operating modules. Links to the
documentation for each module are listed below.  For end-users, the
DOLFIN, mshr and UFL documentation is most relevant.

* `DOLFIN <https://fenics.readthedocs.org/projects/dolfin/en/latest/>`_
* `mshr <https://fenics.readthedocs.org/projects/mshr/en/latest/>`_
* `UFL <https://fenics.readthedocs.org/projects/ufl/en/latest/>`_
* `FFC <https://fenics.readthedocs.org/projects/ffc/en/latest/>`_
* `FIAT <https://fenics.readthedocs.org/projects/fiat/en/latest/>`_
* `dijitso <https://fenics.readthedocs.org/projects/dijitso/en/latest/>`_


Documentation build status
--------------------------

+--------+-------------------------------------------------------------------------------------+
|Main    |   .. image:: https://readthedocs.org/projects/fenics/badge/?version=latest          |
|        |      :target: http://fenics.readthedocs.io/en/latest/?badge=latest                  |
|        |      :alt: Documentation Status                                                     |
+--------+-------------------------------------------------------------------------------------+
|DOLFIN  |  .. image:: https://readthedocs.org/projects/fenics-dolfin/badge/?version=latest    |
|        |     :target: http://fenics.readthedocs.io/projects/dolfin/en/latest/?badge=latest   |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+
|mshr    |  .. image:: https://readthedocs.org/projects/fenics-mshr/badge/?version=latest      |
|        |     :target: http://fenics.readthedocs.io/projects/mshr/en/latest/?badge=latest     |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+
|UFL     |  .. image:: https://readthedocs.org/projects/fenics-ufl/badge/?version=latest       |
|        |     :target: http://fenics.readthedocs.io/projects/ufl/en/latest/?badge=latest      |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+
|FFC     |  .. image:: https://readthedocs.org/projects/fenics-ffc/badge/?version=latest       |
|        |     :target: http://fenics.readthedocs.io/projects/ffc/en/latest/?badge=latest      |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+
|FIAT    |  .. image:: https://readthedocs.org/projects/fenics-fiat/badge/?version=latest      |
|        |     :target: http://fenics.readthedocs.io/projects/fiat/en/latest/?badge=latest     |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+
|dijitso |  .. image:: https://readthedocs.org/projects/fenics-dijitso/badge/?version=latest   |
|        |     :target: http://fenics.readthedocs.io/projects/dijitso/en/latest/?badge=latest  |
|        |     :alt: Documentation Status                                                      |
+--------+-------------------------------------------------------------------------------------+
