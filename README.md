# FEniCS documentation

This is the documentation of FEniCS for users and umbrella for FEniCS
components. It is hosted on Read the Docs
(http://fenics.readthedocs.org/).

This repository is for documentation that address FEniCS as a
whole. Individual projects has their own documentation, the source of
which resides the project repository. Links to the online
documentation for each project can be found at
http://fenics.readthedocs.org/.

This documentation is under development.

[![Documentation Status](https://readthedocs.org/projects/fenics/badge/?version=latest)](http://fenics.readthedocs.io/en/latest/?badge=latest)
